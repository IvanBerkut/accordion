import React, { useReducer } from 'react'
import cx from 'classnames'
import Chevron from './Chevron'
import Completed from './Completed'
import './Stepper.css'

function Stepper({steps}) {

  const initialState = {currentStep: 0, skippedSteps: new Set()};

  const ACTION_TYPES = {
    NEXT_STEP: 'nextStep',
    PREV_STEP: 'prevStep',
    SKIP_STEP: 'skipStep',
    RESET_STEP: 'resetStep'
  }

  function reducer(state, action) {
    switch (action.type) {
      case ACTION_TYPES.NEXT_STEP:
        state.skippedSteps.delete(state.currentStep);
        return {currentStep: state.currentStep + 1, skippedSteps: state.skippedSteps};
      case ACTION_TYPES.PREV_STEP:
        state.skippedSteps.delete(state.currentStep);
        return {currentStep: state.currentStep - 1, skippedSteps: state.skippedSteps};
      case ACTION_TYPES.SKIP_STEP:
        state.skippedSteps.add(state.currentStep)
        return {currentStep: state.currentStep + 1, skippedSteps: state.skippedSteps};
      case ACTION_TYPES.RESET_STEP:
        return {currentStep: 0, skippedSteps: new Set()};
      default:
        return {currentStep: state.currentStep, skippedSteps: state.skippedSteps};
    }
  }

  const [state, dispatch] = useReducer(reducer, initialState);

      return (
          <div className="container">

              <div className="step-progress-bar">
                  {
                  steps.map((item, index) => (
                    <div className="step" key={index}>
                        <div className={cx('bullet', {active: index === state.currentStep, completed: index < state.currentStep && !state.skippedSteps.has(index)})}>
                          {index < state.currentStep && !state.skippedSteps.has(index) ? <Chevron width={12} fill="#fff" /> : index + 1}
                          </div>
                        <div className={cx('step-text', {active: index === state.currentStep, completed: index < state.currentStep && !state.skippedSteps.has(index)})}>{item.text}</div>
                    </div>
                ))
                }
              </div>

              <div>
                <div className="step-content">
                  {state.currentStep < steps.length ? React.createElement(steps[state.currentStep].contentComponents) : <Completed />}
                </div>
                <div className="buttons">
                    {state.currentStep < steps.length ? <button onClick={() => dispatch({type: ACTION_TYPES.PREV_STEP})} className="secondary" disabled={state.currentStep < 1}>Back</button> : null}
                    {steps[state.currentStep] && steps[state.currentStep].optional ? <button onClick={() => dispatch({type: ACTION_TYPES.SKIP_STEP})}>Skip</button> : null}
                    {state.currentStep < steps.length - 1 ? <button onClick={() => dispatch({type: ACTION_TYPES.NEXT_STEP})}>Next</button> : null}
                    {state.currentStep === steps.length - 1 ? <button onClick={() => dispatch({type: ACTION_TYPES.NEXT_STEP})}>Finish</button> : null}
                    {state.currentStep > steps.length - 1 ? <button onClick={() => dispatch({type: ACTION_TYPES.RESET_STEP})} className="secondary">Reset</button> : null}
                </div>

            </div>
        </div>
    )
}


export default Stepper