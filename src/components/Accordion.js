import React, { useState, useRef } from 'react'
import './Accordion.css'
import Chevron from './Chevron'

function Accordion({ disabled, content, title }){
    
    const [isActive, setActiveState] = useState(false)

    const contentRef = useRef({})

    function toggleAccordion() {
        setActiveState((v) => !v)
    }

    const activeClass = isActive ? "active" : "";
    const chevronClass = isActive ? "accordion__icon" : "accordion__icon rotate";
    const disabledClass = disabled ? "accordion__disabled" : "";
    const itemHeight = !isActive 
        ? 0 
        : contentRef.current
          ? contentRef.current.scrollHeight
          : 0;

    return(
        <div className={`accordion__section ${activeClass}`}>
            <button className={`accordion ${activeClass} ${disabledClass}`} onClick={toggleAccordion} disabled={disabled}>
                <p className='accordion__title'>{title}</p>
                <Chevron className={`${chevronClass}`} width={10} fill={'#777'}/>
            </button>
            <div ref={contentRef} style={{maxHeight: `${itemHeight}px`}} className='accordion__content'>
                <div className='accordion__text'>
                    {content}
                </div>
            </div>
        </div>
    )
}

export default Accordion
