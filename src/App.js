import React from 'react';
import './App.css';
import Accordion from './components/Accordion'
import Stepper from './components/Stepper/Stepper'
import StepOneContent from './components/Stepper/StepOneContent'
import StepTwoContent from './components/Stepper/StepTwoContent'
import StepThreeContent from './components/Stepper/StepThreeContent'

const ACCORDIONS = [
  { title: "Accordion 1", value: 1, disabled: false, content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.'},
  { title: "Accordion 2", value: 2, disabled: false, content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.'},
  { title: "Disabled Accordion", value: 3, disabled: true, content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.'}
];

const steps = [
  {text: "Select campaign settings", optional: false, contentComponents: StepOneContent },
  {text: "Create an ad group", optional: true, contentComponents: StepTwoContent },
  {text: "Create an ad", optional: false, contentComponents: StepThreeContent }
]


function App() {

  const accordionArray = ACCORDIONS.map((item) => <Accordion title={item.title} content={item.content} disabled={item.disabled} key={item.value}/> )

  return (
    <div className="App">

      <div className="accordion_section">{accordionArray}</div>

      <div className="stepper-container-horizontal">
      <Stepper steps={steps} />
      </div>

    </div>
  );
}

export default App;
